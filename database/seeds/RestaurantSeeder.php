<?php

use App\Restaurant;
use App\RestaurantHoraire;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $res1 = Restaurant::create(['name' => 'Chilis']);

        /**
         ******* Lundi *******
         **/
        RestaurantHoraire::create([
            'jour'              => 'Lundi',
            'creneau_horaire'   => 'Soir',
            'date_debut'        => Carbon::createFromTime(18, 30)->format('H:i'),
            'date_fin'          => Carbon::createFromTime(20, 0)->format('H:i'),
            'restaurant_id'     => $res1->id,
        ]);

        /**
         ******* Mardi *******
         **/
        RestaurantHoraire::create([
            'jour'              => 'Mardi',
            'creneau_horaire'   => 'Midi',
            'date_debut'        => Carbon::createFromTime(12, 0)->format('H:i'),
            'date_fin'          => Carbon::createFromTime(14, 0)->format('H:i'),
            'restaurant_id'     => $res1->id,
        ]);
        RestaurantHoraire::create([
            'jour'              => 'Mardi',
            'creneau_horaire'   => 'Soir',
            'date_debut'        => Carbon::createFromTime(18, 30)->format('H:i'),
            'date_fin'          => Carbon::createFromTime(20, 0)->format('H:i'),
            'restaurant_id'     => $res1->id,
        ]);

        /**
         ******* Mercredi *******
         **/
        RestaurantHoraire::create([
            'jour'              => 'Mercredi',
            'creneau_horaire'   => 'Midi',
            'date_debut'        => Carbon::createFromTime(12, 0)->format('H:i'),
            'date_fin'          => Carbon::createFromTime(14, 0)->format('H:i'),
            'restaurant_id'     => $res1->id,
        ]);
        RestaurantHoraire::create([
            'jour'              => 'Mercredi',
            'creneau_horaire'   => 'Soir',
            'date_debut'        => Carbon::createFromTime(18, 30)->format('H:i'),
            'date_fin'          => Carbon::createFromTime(20, 0)->format('H:i'),
            'restaurant_id'     => $res1->id,
        ]);

        /**
         ******* Jeudi *******
         **/
        RestaurantHoraire::create([
            'jour'              => 'Jeudi',
            'creneau_horaire'   => 'Midi',
            'date_debut'        => Carbon::createFromTime(12, 0)->format('H:i'),
            'date_fin'          => Carbon::createFromTime(14, 0)->format('H:i'),
            'restaurant_id'     => $res1->id,
        ]);
        RestaurantHoraire::create([
            'jour'              => 'Jeudi',
            'creneau_horaire'   => 'Soir',
            'date_debut'        => Carbon::createFromTime(18, 30)->format('H:i'),
            'date_fin'          => Carbon::createFromTime(20, 0)->format('H:i'),
            'restaurant_id'     => $res1->id,
        ]);

        /**
         ******* Vendredi *******
         **/
        RestaurantHoraire::create([
            'jour'              => 'Vendredi',
            'creneau_horaire'   => 'Midi',
            'date_debut'        => Carbon::createFromTime(12, 0)->format('H:i'),
            'date_fin'          => Carbon::createFromTime(14, 0)->format('H:i'),
            'restaurant_id'     => $res1->id,
        ]);
        RestaurantHoraire::create([
            'jour'              => 'Vendredi',
            'creneau_horaire'   => 'Soir',
            'date_debut'        => Carbon::createFromTime(18, 30)->format('H:i'),
            'date_fin'          => Carbon::createFromTime(20, 0)->format('H:i'),
            'restaurant_id'     => $res1->id,
        ]);

        /**
         ******* Samedi *******
         **/
        RestaurantHoraire::create([
            'jour'              => 'Samedi',
            'creneau_horaire'   => 'Midi',
            'date_debut'        => Carbon::createFromTime(12, 0)->format('H:i'),
            'date_fin'          => Carbon::createFromTime(14, 0)->format('H:i'),
            'restaurant_id'     => $res1->id,
        ]);
        RestaurantHoraire::create([
            'jour'              => 'Samedi',
            'creneau_horaire'   => 'Soir',
            'date_debut'        => Carbon::createFromTime(18, 30)->format('H:i'),
            'date_fin'          => Carbon::createFromTime(20, 0)->format('H:i'),
            'restaurant_id'     => $res1->id,
        ]);
    }
}
