## Projet : Horaire Restaurants 


## Chargés
- [Tayari Marouéne](mailto:tayari_marouene@yahoo.fr), Développeur PHP - Laravel

## Versions
- PHP 7.0
- Laravel 5.5

## IDE
- PHPStorm 2017.3

## Installation 

Voici donc les étapes à réaliser pour démarrer ce projet Laravel.

**1-** Ouvrer votre terminal si vous avez un environnement (Linux ou Mac OS) ou bien si vous êtes sur un environnement Windows vous pouvez utiliser GitBash

**2-** Cloner le projet via la commande : 
``` 
git clone git@gitlab.com:Mriwech/horaires-des-restaurants.git 
```
**3-** Vérifier les configurations dans le fichier 
``` 
config\database.php 
```
**4-** Créez la base de donnée dans votre environnement    
>**horaires_restaurants**

**5-A** Lancer la création des tables de la base des données avec la commande : 
``` 
php artisan migrate
```
**5-B** Lancer les seeders avec la commande : 
``` 
php artisan db:seed
```
**6** Par la suite vous pouvez consulter le résultat dans le lien : 
``` 
http://localhost/horaires_restaurants/public/restaurant/1
```