<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RestaurantHoraire extends Model
{
    protected $table = 'restaurant_horaires';

    public $timestamps = true;

    protected $fillable = ['jour', 'creneau_horaire', 'date_debut', 'date_fin', 'restaurant_id'];

    public function getDateDebutAttribute($value)
    {
        return Carbon::createFromFormat('H:i:s',$value)->format('H:i');
    }

    public function getDateFinAttribute($value)
    {
        return Carbon::createFromFormat('H:i:s',$value)->format('H:i');
    }
}
