<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $table = 'restaurants';

    public $timestamps = true;

    protected $fillable = ['name'];

    public function horaires(){
        return $this->hasMany('App\RestaurantHoraire', 'restaurant_id', 'id');
    }

    public function horairesParJour($jour){
        return $this->hasMany('App\RestaurantHoraire', 'restaurant_id', 'id')->where('jour', $jour);
    }
}
