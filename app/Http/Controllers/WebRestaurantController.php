<?php

namespace App\Http\Controllers;

use App\Restaurant;
use Illuminate\Http\Request;

class WebRestaurantController extends Controller
{
    public function showRestaurant($id)
    {

        $restaurant = Restaurant::find($id);

        return view('showRestaurant',
                    [
                        'jours'         => ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'],
                        'restaurant'    => $restaurant
                    ]
                )
            ;
    }
}
