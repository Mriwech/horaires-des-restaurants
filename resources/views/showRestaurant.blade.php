<html>
<head>
    <title></title>
</head>
<body>
Bonjour dans le Restaurant, "{{ $restaurant->name }}"<br><br><br>


<table>
    @foreach($jours as $jour)
        <tr>
            <td> {{ $jour }} :</td>
            @if($restaurant->horairesParJour($jour)->count() > 0)
                @foreach($restaurant->horairesParJour($jour)->orderBy('date_debut')->get() as $key => $horaire)
                    <td>
                        {{ $horaire->date_debut . ' - ' . $horaire->date_fin }}
                        {{ ($key < $restaurant->horairesParJour($jour)->count()-1) ? ',' : null }}
                    </td>
                @endforeach
            @else
                <td>Fermé</td>
            @endif
        </tr>
    @endforeach
</table>

</body>
</html>